using EquityBankAssessment.Transactions.DataAccess;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EquityBankAssessment.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            //Find the service layer within our scope.
            using (var scope = host.Services.CreateScope())
            {
                //Get the instance of TransactionsDBContext in our services layer
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<TransactionsDBContext>();

                //Call the DataGenerator to create sample data
                DataGenerator.Initialize(services);
            }
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
