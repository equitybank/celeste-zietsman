using System;
using System.Threading.Tasks;
using EquityBankAssessment.Transactions.Domain.Services.Interfaces;
using EquityBankAssessment.Transactions.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EquityBankAssessment.WebApi.Controllers
{
    [Authorize]
    [Route("v1")]
    [ApiController]
    public class LimitController : ControllerBase
    {
        private readonly ILimitService _limitService;
        //TODO: Add logging
        public LimitController(ILimitService limitService)
        {
            this._limitService = limitService;
        }

        [Route("getlimit")]
        [HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LimitResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetTransactionLimit([FromBody] LimitRequest limitRequest)
        {
            try
            {
                var result = await _limitService.GetTransactionLimitAsync(limitRequest);

                if (result != null)
                return StatusCode(StatusCodes.Status200OK, result);
                else
                return StatusCode(StatusCodes.Status404NotFound, result);
            }
            catch (Exception e)
            {
                return StatusCode((int)StatusCodes.Status500InternalServerError, e.Message);
            }
        }


        [Route("globallimit")]
        [HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LimitResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateGlobalLimit([FromBody] GlobalLimitRequest globalLimitRequest)
        {
            try
            {
                var result = await _limitService.CreateGlobalLimitAsync(globalLimitRequest);

                if (result != null)
                    return StatusCode(StatusCodes.Status200OK, result);
                else
                    return StatusCode(StatusCodes.Status404NotFound, result);
            }
            catch (Exception e)
            {
                return StatusCode((int)StatusCodes.Status500InternalServerError, e.Message);
            }
        }


        [Route("transactionlimit")]
        [HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LimitResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateTransactionLimit([FromBody] TransactionLimitRequest transactionLimitRequest)
        {
            try
            {
                var result = await _limitService.CreateTransactionLimitAsync(transactionLimitRequest);
                
                if (result != null)
                    return StatusCode(StatusCodes.Status200OK, result);
                else
                    return StatusCode(StatusCodes.Status404NotFound, result);
            }
            catch (Exception e)
            {
                return StatusCode((int)StatusCodes.Status500InternalServerError, e.Message);
            }
        }

    }
}