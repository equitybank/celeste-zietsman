**Notes**

**Data Access**

InMemory database has been used to store and retrieve data. A test transaction limit record is generator on startup.


**JWT Authentication and Authorization**

The use of JWT authentication was used. To get an access token to authenticate a user i have decide to go with azure ad b2c to generate a token to allow authorization to the user. 
If setup is in place, it's easy to just replace the configuration values in the appsettings.json file.


**Validation**

I have decided to use data annotations for the field validations.


**API documentation**

Swagger documentation was used to document the endpoints and the response codes as well as the example request body


**Testing the application**

To run the app in visual studio just hit F5. To run the app in VS Code just execute the following command "dotnet run".
Testing the api as is without a correct bearer token will result in an 401 "Unauthorized" error. This is because of the [Authorize] attribute on the controller class.
To test without authentication just comment out the [Authorize] attribute.
I have tested the authentication with postman and it worked fine but I could not get it to work with the swagger authorize. Not enough time to test and debug the issue.