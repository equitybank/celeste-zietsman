using EquityBankAssessment.Transactions.DataAccess.Interfaces;
using EquityBankAssessment.Transactions.Domain.Services.Interfaces;
using EquityBankAssessment.Transactions.Model;
using System;
using System.Threading.Tasks;

namespace EquityBankAssessment.Transactions.Logic
{
    public class LimitService : ILimitService
    {
        private readonly ILimitDataAccess _limitDataAccess;
        private const string _successCode = "0";
        private const string _failedCode = "1";
        private const string _successDescription = "Request Processed Successfully";

        public LimitService(ILimitDataAccess limitDataAccess)
        {
            _limitDataAccess = limitDataAccess;
        }

        public async Task<LimitResponse> CreateGlobalLimitAsync(GlobalLimitRequest globalLimitRequest)
        {
            if (globalLimitRequest == null) throw new ArgumentNullException($"{nameof(GlobalLimitRequest)}", $"{nameof(GlobalLimitRequest)} cannot be null. Please provide the request values.");

            var result = await Task.Run(() =>
            {
                var limitResponse = new LimitResponse();
                limitResponse.RId = globalLimitRequest.RId;
                var globalLimit = _limitDataAccess.GetGlobalLimit(globalLimitRequest.ProductType);
                //If limit is null then add to the database 
                //Did not check if it needs to be updated because spec is not clear. Only says to create.
                if (globalLimit == null)
                {
                    if (_limitDataAccess.AddGlobalLimit(globalLimitRequest) > 0)
                        limitResponse = UpdateGlobalLimitResponse(limitResponse, _successCode, _successDescription,
                                                                     globalLimitRequest);

                }
                return limitResponse;
            });
            return result;
        }

        public async Task<LimitResponse> CreateTransactionLimitAsync(TransactionLimitRequest transactionLimitRequest)
        {
            if (transactionLimitRequest == null) throw new ArgumentNullException($"{nameof(TransactionLimitRequest)}", $"{nameof(TransactionLimitRequest)} cannot be null. Please provide the request values.");

            var result = await Task.Run(() =>
            {
                var limitResponse = new LimitResponse();
                limitResponse.RId = transactionLimitRequest.RId;
                var transactionLimit = _limitDataAccess.GetTransactionLimit(transactionLimitRequest.AccountId);
                //If limit is null then add to the database 
                //Did not check if it needs to be updated because spec is not clear. Only says to create.
                if (transactionLimit == null)
                {
                    if (_limitDataAccess.AddTransactionLimit(transactionLimitRequest) > 0)
                        limitResponse = UpdateTransactionLimitResponse(limitResponse, _successCode, _successDescription,
                                                                    transactionLimitRequest);

                }
                return limitResponse;
            });
            return result;
        }

        public async Task<LimitResponse> GetTransactionLimitAsync(LimitRequest limitRequest)
        {
            if (limitRequest == null) throw new ArgumentNullException($"{nameof(LimitRequest)}", $"{nameof(LimitRequest)} cannot be null. Please provide the request values.");
            
            var result = await Task.Run(() =>
            {
                var limitResponse = new LimitResponse();
                limitResponse.RId = limitRequest.RId;
                var transactionlimit = _limitDataAccess.GetTransactionLimit(limitRequest.AccountId);
                if (transactionlimit == null) return null;
               
                UpdateTransactionLimitResponse(limitResponse, _successCode, _successDescription,
                                                                transactionlimit);
                return limitResponse;
            });
            return result;
        }

        private LimitResponse UpdateGlobalLimitResponse(LimitResponse limitResponse, string statusCode, string description,
                                                          GlobalLimitRequest globalLimit)
        {
            limitResponse.Status.Code = statusCode;
            limitResponse.Status.Description = description;
            limitResponse.Limit.DailyLimit = (globalLimit == null) ?  string.Empty : globalLimit.DailyLimit;
            limitResponse.Limit.MonthlyLimit = (globalLimit == null) ? string.Empty : globalLimit.MonthlyLimit;
            limitResponse.Limit.WeeklyLimit = (globalLimit == null) ? string.Empty : globalLimit.WeeklyLimit;
            limitResponse.Limit.YearlyLimit = (globalLimit == null) ? string.Empty : globalLimit.YearlyLimit;
            limitResponse.Limit.TransactionLimit = (globalLimit == null) ? string.Empty : globalLimit.TransactionLimit;
            return limitResponse;
        }
        private LimitResponse UpdateTransactionLimitResponse(LimitResponse limitResponse, string statusCode, string description,
                                                            TransactionLimitRequest transactionLimit)
        {
            limitResponse.Status.Code = statusCode;
            limitResponse.Status.Description = description;
            limitResponse.Limit.DailyLimit = (transactionLimit == null) ? string.Empty : transactionLimit.DailyLimit;
            limitResponse.Limit.MonthlyLimit = (transactionLimit == null) ? string.Empty : transactionLimit.MonthlyLimit;
            limitResponse.Limit.WeeklyLimit = (transactionLimit == null) ? string.Empty : transactionLimit.WeeklyLimit;
            limitResponse.Limit.YearlyLimit = (transactionLimit == null) ? string.Empty : transactionLimit.YearlyLimit;
            limitResponse.Limit.TransactionLimit = (transactionLimit == null) ? string.Empty : transactionLimit.TransactionLimit;
            return limitResponse;
        }
    }
   
}
