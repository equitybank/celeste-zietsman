using AutoMapper;
using AutoMapper.Configuration;
using EquityBankAssessment.Transactions.DataAccess.Interfaces;
using EquityBankAssessment.Transactions.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Transactions;
using Unity;

namespace EquityBankAssessment.Transactions.DataAccess.Tests
{
    //TODO: Add DataAccess Tests
    public class LimitDataAccessTests
    {
        private readonly ILimitDataAccess _limitDataAccess;
        private readonly IUnityContainer _unityContainer;
        public readonly Microsoft.Extensions.Configuration.IConfiguration Configuration;
        DbContextOptions<TransactionsDBContext> options;

        public LimitDataAccessTests()
        {
            // Initialize AutoMapper in test class
            var myProfile = new AutoMapping();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            IMapper mapper = configuration.CreateMapper();

            this._unityContainer = new UnityContainer();
            _unityContainer.RegisterInstance(mapper);
            _unityContainer.RegisterType<ILimitDataAccess, LimitDataAccess>();
            _limitDataAccess = _unityContainer.Resolve<ILimitDataAccess>();

            var builder = new DbContextOptionsBuilder<TransactionsDBContext>();
            builder.UseInMemoryDatabase(databaseName: "Transactions");
            options = builder.Options;
      
        }

        [Test]
        public void GetTransactionLimit_ReturnValidObject()
        {
            using (TransactionScope scope = new TransactionScope())
            {
                LimitRequest request = new LimitRequest
                {
                    RId = "1",    
                    AccountId = "01701934343512",    
                    SchemeCode = "LA324",    
                    Channel = "Way4",    
                    CountryCode = "KE",    
                    Currency = "KES",    
                    BankCode = "54"
                };
                var response = _limitDataAccess.GetTransactionLimit(request.AccountId);
              
                Assert.IsNotNull(response);
            }
        }
    }
}