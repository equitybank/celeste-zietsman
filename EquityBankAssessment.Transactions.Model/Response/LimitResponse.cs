using System.Text.Json.Serialization;

namespace EquityBankAssessment.Transactions.Model
{
    public class LimitResponse
    {
        public string RId { get; set; }
        /// <summary>
        /// Status holds the Code and Description as shown in the sample response.
        /// </summary>
        public Status Status { get; set; } = new Status();
        public Limit Limit { get; set; } = new Limit();
    }
}
