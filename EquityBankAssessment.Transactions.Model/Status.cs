﻿
namespace EquityBankAssessment.Transactions.Model
{
    public class Status
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
