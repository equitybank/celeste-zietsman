
namespace EquityBankAssessment.Transactions.Model
{
    public class Limit
    {
        public string DailyLimit { get; set; }
        public string WeeklyLimit { get; set; }
        public string MonthlyLimit { get; set; }
        public string YearlyLimit { get; set; }
        public string TransactionLimit { get; set; }
    }
}
