using System.ComponentModel.DataAnnotations;

namespace EquityBankAssessment.Transactions.Model
{
    public class GlobalLimitRequest : BaseRequest
    {
        [Required]
        public string ProductType { get; set; }
        [Required]
        public string DailyLimit { get; set; }
        [Required]
        public string WeeklyLimit { get; set; }
        [Required]
        public string MonthlyLimit { get; set; }
        [Required]
        public string YearlyLimit { get; set; }
        [Required]
        public string TransactionLimit { get; set; }
    }
}
