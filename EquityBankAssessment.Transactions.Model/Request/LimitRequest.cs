using System.ComponentModel.DataAnnotations;

namespace EquityBankAssessment.Transactions.Model
{
    public class LimitRequest : BaseRequest
    {
        [Required]
        public string AccountId { get; set; }
    }
}
