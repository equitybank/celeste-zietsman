﻿using System.ComponentModel.DataAnnotations;

namespace EquityBankAssessment.Transactions.Model
{
    public class BaseRequest
    {
        [Required]
        public string RId { get; set; }
        [Required]
        public string SchemeCode { get; set; }
        [Required]
        public string Channel { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string BankCode { get; set; }
    }
}
