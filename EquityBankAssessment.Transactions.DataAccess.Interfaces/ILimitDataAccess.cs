﻿using EquityBankAssessment.Transactions.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EquityBankAssessment.Transactions.DataAccess.Interfaces
{
    public interface ILimitDataAccess
    {
        TransactionLimitRequest GetTransactionLimit(string accountId);
        GlobalLimitRequest GetGlobalLimit(string productType);
        int AddGlobalLimit(GlobalLimitRequest globalLimit);
        int AddTransactionLimit(TransactionLimitRequest transactionLimit);

    }
}
