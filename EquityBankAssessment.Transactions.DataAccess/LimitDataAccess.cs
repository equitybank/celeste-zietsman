﻿using AutoMapper;
using EquityBankAssessment.Transactions.DataAccess.Interfaces;
using EquityBankAssessment.Transactions.Model;
using System.Linq;

namespace EquityBankAssessment.Transactions.DataAccess
{
    public class LimitDataAccess : ILimitDataAccess
    {
        private readonly IMapper _mapper;
        private readonly TransactionsDBContext _transactionDBContext;
        public LimitDataAccess(IMapper mapper, TransactionsDBContext context)
        {
            _mapper = mapper;
            _transactionDBContext = context;
        }     

        /// <summary>
        /// Get the TransactionLimit by AccountId
        /// Assumming that the accountId would be unique
        /// </summary>
        /// <param name="limitRequest"></param>
        /// <returns></returns>
        public Model.TransactionLimitRequest GetTransactionLimit(string accountId)
        {
            var query = _transactionDBContext.TransactionLimit.FirstOrDefault(x => x.AccountId.ToLowerInvariant() == accountId.ToLowerInvariant());
                
            return _mapper.Map<Model.TransactionLimitRequest>(query);
        }

        /// <summary>
        /// Get the Global limit by productType
        /// Assumming that the product type would be unique
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        public Model.GlobalLimitRequest GetGlobalLimit(string productType)
        {
            var query = _transactionDBContext.GlobalLimit.FirstOrDefault(x => x.ProductType.ToLowerInvariant() == productType.ToLowerInvariant());

            return _mapper.Map<Model.GlobalLimitRequest>(query);
        }

        /// <summary>
        /// Add the GlobalLimit to the database
        /// </summary>
        /// <param name="globalLimit"></param>
        public int AddGlobalLimit(Model.GlobalLimitRequest globalLimit)
        {
            Models.GlobalLimit dbEntry = _mapper.Map<Models.GlobalLimit>(globalLimit);
            _transactionDBContext.GlobalLimit.Add(dbEntry);
            _transactionDBContext.SaveChanges();
            return dbEntry.Id;
        }

        /// <summary>
        /// Add the TransactionLimit to the database
        /// </summary>
        /// <param name="transactionLimit"></param>
        /// <returns></returns>
        public int AddTransactionLimit(TransactionLimitRequest transactionLimit)
        {
            Models.Transaction dbEntry = _mapper.Map<Models.Transaction>(transactionLimit);
            _transactionDBContext.TransactionLimit.Add(dbEntry);
            _transactionDBContext.SaveChanges();
            return dbEntry.Id;
        }
    }
}
