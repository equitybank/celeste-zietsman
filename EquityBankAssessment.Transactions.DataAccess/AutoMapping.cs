﻿using AutoMapper;
using EquityBankAssessment.Transactions.DataAccess.Models;
using EquityBankAssessment.Transactions.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBankAssessment.Transactions.DataAccess
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            // means you want to map from Database Object to DTO's
            CreateMap<Models.Transaction, Model.TransactionLimitRequest>().ReverseMap();
            CreateMap<Models.GlobalLimit, Model.GlobalLimitRequest>().ReverseMap();
        }
    }
}
