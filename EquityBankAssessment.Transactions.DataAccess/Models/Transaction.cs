using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EquityBankAssessment.Transactions.DataAccess.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }
     
        public string AccountId { get; set; }
        public string SchemeCode { get; set; }
        public string Channel { get; set; }
        public string CountryCode { get; set; }
        public string Currency { get; set; }
        public string BankCode { get; set; }
        public string ProductType { get; set; }
        public string DailyLimit { get; set; }
        public string WeeklyLimit { get; set; }
        public string MonthlyLimit { get; set; }
        public string YearlyLimit { get; set; }
        public string TransactionLimit { get; set; }

    }
}
