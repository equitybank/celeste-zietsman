using Microsoft.EntityFrameworkCore;
using System;

namespace EquityBankAssessment.Transactions.DataAccess
{
    public class TransactionsDBContext : DbContext
    {
        public TransactionsDBContext()
        {
        }

        public TransactionsDBContext(DbContextOptions<TransactionsDBContext> options)
            : base(options) { }

        public DbSet<Models.Transaction> TransactionLimit { get; set; }
        public DbSet<Models.GlobalLimit> GlobalLimit { get; set; }
    }
}
