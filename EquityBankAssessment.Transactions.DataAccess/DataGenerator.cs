using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquityBankAssessment.Transactions.DataAccess
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new TransactionsDBContext(
                serviceProvider.GetRequiredService<DbContextOptions<TransactionsDBContext>>()))
            {
                // Look for any transactions
                if (context.TransactionLimit.Any())
                {
                    return;   // Data was already seeded
                }

                context.TransactionLimit.AddRange(
                    new Models.Transaction
                    {                      
                        Id = 1,
                        AccountId = "01701934343512",
                        SchemeCode = "LA324",
                        Channel = "Way4",
                        CountryCode = "KE",
                        Currency = "KES",
                        BankCode = "54",
                        ProductType = "",
                        DailyLimit = "1000000",
                        WeeklyLimit = "1000000",
                        MonthlyLimit = "1000000",
                        YearlyLimit = "1000000",
                        TransactionLimit = "1000000"

                    }); ;
                 

                context.SaveChanges();
            }
        }
    }
}
