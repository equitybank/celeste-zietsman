using EquityBankAssessment.Transactions.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EquityBankAssessment.Transactions.Domain.Services.Interfaces
{
    public interface ILimitService
    {
        /// <summary>
        /// Gets a transaction limit async
        /// </summary>
        /// <param name="transactionLimitRequest">TransactionLimitRequest</param>
        /// <returns><LimitResponse></returns>
        Task<LimitResponse> GetTransactionLimitAsync(LimitRequest limitRequest);

        /// <summary>
        /// Creates a transaction limit async
        /// </summary>
        /// <param name="transactionLimitRequest">TransactionLimitRequest</param>
        /// <returns>IEnumerable<LimitResponse></returns>
        Task<LimitResponse> CreateTransactionLimitAsync(TransactionLimitRequest transactionLimitRequest);

        /// <summary>
        /// Creates a global limit async
        /// </summary>
        /// <param name="globalLimitRequest">GlobalLimitRequest</param>
        /// <returns>IEnumerable<LimitResponse></returns>
        Task<LimitResponse> CreateGlobalLimitAsync(GlobalLimitRequest globalLimitRequest);


    }
}
