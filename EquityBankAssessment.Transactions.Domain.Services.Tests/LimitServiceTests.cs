using EquityBankAssessment.Transactions.DataAccess;
using EquityBankAssessment.Transactions.DataAccess.Interfaces;
using EquityBankAssessment.Transactions.Domain.Services.Interfaces;
using EquityBankAssessment.Transactions.Logic;
using EquityBankAssessment.Transactions.Model;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Unity;

namespace EquityBankAssessment.Transactions.Domain.Services.Tests
{
    public class LimitServiceTests
    {
        private readonly Mock<ILimitDataAccess> _mockDataAccess;
        private readonly ILimitService _limitService;
        private readonly IUnityContainer _unityContainer;
        private const string _responseCode = "0";
        private const string _responseDescription = "Request Processed Successfully";

        public LimitServiceTests()
        {
            _mockDataAccess = new Mock<ILimitDataAccess>(MockBehavior.Strict);
            this._unityContainer = new UnityContainer();

            _unityContainer.RegisterInstance(_mockDataAccess.Object);
            _unityContainer.RegisterType<ILimitService, LimitService>();
            _limitService = _unityContainer.Resolve<ILimitService>();
        }

        #region GetTransactionLimit Tests
        [Test]
        public async Task GetTransactionLimit_ReturnSuccessCode()
        {
            LimitRequest limitRequest = new LimitRequest
            {
                RId = "1",    
                AccountId = "01701934343512",    
                SchemeCode = "LA324",    
                Channel = "Way4",    
                CountryCode = "KE",    
                Currency = "KES",    
                BankCode = "54" 
            };
            TransactionLimitRequest transactionLimitRequest = new TransactionLimitRequest();
            _mockDataAccess.Setup(x => x.GetTransactionLimit(It.IsAny<string>())).Returns(transactionLimitRequest);
            var result = await _limitService.GetTransactionLimitAsync(limitRequest);
            _mockDataAccess.Verify(x => x.GetTransactionLimit(It.IsAny<string>()), Times.Once);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Status.Code == _responseCode);
            Assert.IsTrue(result.Status.Description == _responseDescription);

        }

        [Test]
        public async Task GetTransactionLimit_ReturnNull()
        {
            LimitRequest limitRequest = new LimitRequest
            {
                RId = "1",
                AccountId = "01701934343512",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54"
            };
            _mockDataAccess.Setup(x => x.GetTransactionLimit(It.IsAny<string>())).Returns((TransactionLimitRequest)null);
            var result = await _limitService.GetTransactionLimitAsync(limitRequest);
            _mockDataAccess.Verify(x => x.GetTransactionLimit(It.IsAny<string>()), Times.Once);

            Assert.IsNull(result);
        }

        [Test]
        public void GetTransactionLimit_ReturnArgumentNullException()
        {
            LimitRequest limitRequest = null;
            var ex = Assert.ThrowsAsync<ArgumentNullException>(async () => await _limitService.GetTransactionLimitAsync(limitRequest));
            Assert.That(ex.Message, Is.EqualTo($"{nameof(LimitRequest)} cannot be null. Please provide the request values. (Parameter '{nameof(LimitRequest)}')"));
            Assert.That(ex.ParamName, Is.EqualTo($"{nameof(LimitRequest)}"));
        }
        #endregion

        #region CreateGlobalLimit Tests
        [Test]
        public void CreateGlobalLimit_ReturnArgumentNullException()
        {
            GlobalLimitRequest globalLimitRequest = null;
            var ex = Assert.ThrowsAsync<ArgumentNullException>(async () => await _limitService.CreateGlobalLimitAsync(globalLimitRequest));
            Assert.That(ex.Message, Is.EqualTo($"{nameof(GlobalLimitRequest)} cannot be null. Please provide the request values. (Parameter '{nameof(GlobalLimitRequest)}')"));
            Assert.That(ex.ParamName, Is.EqualTo($"{nameof(GlobalLimitRequest)}"));
        }

        [Test]
        public async Task CreateGlobalLimit_ReturnSuccessCode()
        {
            GlobalLimitRequest globalLimitRequest = new GlobalLimitRequest
            {
                RId = "1",
                ProductType = "01701934343512",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54",
                DailyLimit = "10000",
                WeeklyLimit = "10000",
                MonthlyLimit = "10000",
                TransactionLimit = "3000",
            };
            _mockDataAccess.Setup(x => x.GetGlobalLimit(It.IsAny<string>())).Returns((GlobalLimitRequest)null);
            _mockDataAccess.Setup(x => x.AddGlobalLimit(globalLimitRequest)).Returns(1);
            var result = await _limitService.CreateGlobalLimitAsync(globalLimitRequest);
            _mockDataAccess.Verify(x => x.GetGlobalLimit(It.IsAny<string>()), Times.Once);
            _mockDataAccess.Verify(x => x.AddGlobalLimit(It.IsAny<GlobalLimitRequest>()), Times.Once);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Status.Code == _responseCode);
            Assert.IsTrue(result.Status.Description == _responseDescription);

        }

        #endregion

        #region CreateTransactionLimit Tests
        [Test]
        public void CreateTransactionLimit_ReturnArgumentNullException()
        {
            TransactionLimitRequest transactionLimitRequest = null;
            var ex = Assert.ThrowsAsync<ArgumentNullException>(async () => await _limitService.CreateTransactionLimitAsync(transactionLimitRequest));
            Assert.That(ex.Message, Is.EqualTo($"{nameof(TransactionLimitRequest)} cannot be null. Please provide the request values. (Parameter '{nameof(TransactionLimitRequest)}')"));
            Assert.That(ex.ParamName, Is.EqualTo($"{nameof(TransactionLimitRequest)}"));
        }

        [Test]
        public async Task CreateTransactionLimit_ReturnSuccessCode()
        {
            TransactionLimitRequest transactionLimitRequest = new TransactionLimitRequest
            {
                RId = "1",
                AccountId = "01701934343512",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54",
                DailyLimit = "10000",
                WeeklyLimit = "10000",
                MonthlyLimit = "10000",
                TransactionLimit = "3000",
            };
            _mockDataAccess.Setup(x => x.GetTransactionLimit(It.IsAny<string>())).Returns((TransactionLimitRequest)null);
            _mockDataAccess.Setup(x => x.AddTransactionLimit(transactionLimitRequest)).Returns(1);
            var result = await _limitService.CreateTransactionLimitAsync(transactionLimitRequest);
            _mockDataAccess.Verify(x => x.GetTransactionLimit(It.IsAny<string>()), Times.Once);
            _mockDataAccess.Verify(x => x.AddTransactionLimit(It.IsAny<TransactionLimitRequest>()), Times.Once);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Status.Code == _responseCode);
            Assert.IsTrue(result.Status.Description == _responseDescription);

        }

        #endregion
    }
}